//#pragma once
#include <opencv2/opencv.hpp>
#include <math.h>
#include <iostream>
#include <fstream>

#include "referenceFrames.h"

using namespace std;

const float CLOSED_GATE_SIZE = 1.28;

//This class is used to solve pnp problem
class PNPSolver
{
public:
    PNPSolver();
    //parameters initial
    PNPSolver(double fx, double fy, double u0, double v0, double k_1, double  k_2, double  p_1, double  p_2, double k_3);
    ~PNPSolver();
 
    enum METHOD
    {
        CV_ITERATIVE,
        CV_P3P,
        CV_EPNP
    };

    /************************************************/
    vector<cv::Point3f> Points3D;
    vector<cv::Point2f> Points2D;
 
    /*************************************************/

    cv::Mat RoteM, TransM;

    cv::Point3f Theta_W2C;

    cv::Point3f Theta_C2W;

    cv::Point3f Position_OwInC;

    cv::Point3f Position_OcInW;
 
 
    /**************************************************/
 
    int Solve(METHOD method = METHOD::CV_P3P);
    void getRotationAndTranslation(cv::Mat& Rvec, cv::Mat& Tvec){Rvec = rvec; Tvec = tvec;}

    vector<cv::Point2f> WordFrame2ImageFrame(vector<cv::Point3f> WorldPoints);
 
 
 

    cv::Point3f ImageFrame2CameraFrame(cv::Point2f p, double F);
 
 
 
 

    void SetCameraMatrix(double fx, double fy, double u0, double v0)
    {
        camera_matrix = cv::Mat(3, 3, CV_64FC1, cv::Scalar::all(0));
        camera_matrix.ptr<double>(0)[0] = fx;
        camera_matrix.ptr<double>(0)[2] = u0;
        camera_matrix.ptr<double>(1)[1] = fy;
        camera_matrix.ptr<double>(1)[2] = v0;
        camera_matrix.ptr<double>(2)[2] = 1.0f;
    }

    void SetDistortionCoefficients(double k_1, double  k_2, double  p_1, double  p_2, double k_3)
    {
        distortion_coefficients = cv::Mat(5, 1, CV_64FC1, cv::Scalar::all(0));
        distortion_coefficients.ptr<double>(0)[0] = k_1;
        distortion_coefficients.ptr<double>(1)[0] = k_2;
        distortion_coefficients.ptr<double>(2)[0] = p_1;
        distortion_coefficients.ptr<double>(3)[0] = p_2;
        distortion_coefficients.ptr<double>(4)[0] = k_3;
    }
 
 
 
 
    void convertToAerostackFrame(double &x, double &y, double &z, double &roll, double &pitch, double &yaw, cv::Mat rvec, cv::Mat tvec);
    void rotateXAxis(cv::Mat &rotation);
    static int createMatHomogFromVecs(cv::Mat& MatHomog, cv::Mat TransVec, cv::Mat RotVec);
    /*****************************************/

    static cv::Point3f RotateByVector(double old_x, double old_y, double old_z, double vx, double vy, double vz, double theta)
    {
        double r = theta * CV_PI / 180;
        double c = cos(r);
        double s = sin(r);
        double new_x = (vx*vx*(1 - c) + c) * old_x + (vx*vy*(1 - c) - vz*s) * old_y + (vx*vz*(1 - c) + vy*s) * old_z;
        double new_y = (vy*vx*(1 - c) + vz*s) * old_x + (vy*vy*(1 - c) + c) * old_y + (vy*vz*(1 - c) - vx*s) * old_z;
        double new_z = (vx*vz*(1 - c) - vy*s) * old_x + (vy*vz*(1 - c) + vx*s) * old_y + (vz*vz*(1 - c) + c) * old_z;
        return cv::Point3f(new_x, new_y, new_z);
    }
 

    static void CodeRotateByZ(double x, double y, double thetaz, double& outx, double& outy)
    {
        double x1 = x;
        double y1 = y;
        double rz = thetaz * CV_PI / 180;
        outx = cos(rz) * x1 - sin(rz) * y1;
        outy = sin(rz) * x1 + cos(rz) * y1;
    }
 

    static void CodeRotateByY(double x, double z, double thetay, double& outx, double& outz)
    {
        double x1 = x;
        double z1 = z;
        double ry = thetay * CV_PI / 180;
        outx = cos(ry) * x1 + sin(ry) * z1;
        outz = cos(ry) * z1 - sin(ry) * x1;
    }
 

    static void CodeRotateByX(double y, double z, double thetax, double& outy, double& outz)
    {
        double y1 = y;
        double z1 = z;
        double rx = thetax * CV_PI / 180;
        outy = cos(rx) * y1 - sin(rx) * z1;
        outz = cos(rx) * z1 + sin(rx) * y1;
    }



private:
 
    cv::Mat camera_matrix;
    cv::Mat distortion_coefficients;
 
    cv::Mat rvec;
    cv::Mat tvec;
};
