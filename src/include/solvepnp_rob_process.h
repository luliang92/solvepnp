#ifndef SOLVEPNP_ROB_PROCESS
#define SOLVEPNP_ROB_PROCESS

//robot process
#include "robot_process.h"

//my own class
#include "FrameDetector.h"
#include "PNPsolver.h"

//I/O Stream
//std::cout
#include <iostream>
#include <string>

//opencv
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <geometry_msgs/PointStamped.h>
#include <cv_bridge/cv_bridge.h>

//tf
#include <tf/tf.h>
#include <tf_conversions/tf_eigen.h>

// ROS
//ros::init(), ros::NodeHandle, ros::ok(), ros::spinOnce()
#include "ros/ros.h"

//msg
#include "solvepnp/FramePoints.h"
#include "solvepnp/FramePoses.h"
#include "aruco_eye_msgs/MarkerList.h"

#include "std_msgs/Int32.h"

class solvepnp_rob_process : public RobotProcess
{

public:

    solvepnp_rob_process(std::string config_file);

    ~solvepnp_rob_process();

    void ownSetUp();
    void ownStart();
    void ownStop();
    void ownRun();
    void init();

    std::string stack_path_;
    std::string drone_id_;
    std::string image_receiver_topic_name;
    std::string camera_info_topic_name;
    std::string pnp_result_topic_name;
    std::string frame_points_topic_name;
    std::string detector_image_topic_name;
    std::string blob_points_topic_name;
    std::string drone_id_namespace;

    std::string solvepnp_config_file;

    FrameDetector framedetector;


private:

    double fx;
    double cx;
    double fy;
    double cy;
    double e1;
    double e2;
    double e3;
    double e4;
    double e5;
  //! ROS NodeHandler used to manage the ROS communication
  ros::NodeHandle n;

  int next_gate_to_cross_;

  //! ROS subscriber handler used to receive messages.
  ros::Subscriber pic_sub;
  ros::Subscriber next_gate_sub;

  void rgbCamInfoCallback(const sensor_msgs::CameraInfo &msg);
  void nextGateToCrossCallback(const std_msgs::Int32 &msg);

  ros::Subscriber camera_para;

  void picCallback(const sensor_msgs::ImageConstPtr& msg);

  //! ROS publish
  ros::Publisher frame_points;
  ros::Publisher blob_points;
  ros::Publisher Re2pnp;
  image_transport::Publisher detector_image_pub;
};
#endif
