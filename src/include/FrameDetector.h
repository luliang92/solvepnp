

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include<vector>
#include <string>
#include <opencv2/imgproc/imgproc.hpp>
#include<cstdlib>
using namespace std;
using namespace cv;

class FrameDetection{ // CORNERS::  0: BOTTOM LEFT, 1:BOTTOM RIGHT, 2: UP RIGHT, 3: UP LEFT
                        //LABELS: CLOSEDGATE, JUNGLEGYM, DYNAMICGATE, UNKNOWN, BLOB 
private:
    vector<Point> corners;
    string label;

public:
    FrameDetection(){corners =  vector<Point> (4, Point(0,0)); label="UNKNOWN";}
    void setLabel(string new_label) { label=new_label;}
    vector<Point> getCorners(void) const {return corners;}
    string getID() const {return label;}
    Point getCorner(int corner) const {return corners[corner];}
    void setCorner(const Point p, int corner ){corners[corner]= p;}
};


class FrameDetector{
private:
    bool show = true, debug=true;
    bool configure=true;
    double tolerance=0.05;
    vector<bool> hasBase;
    vector<FrameDetection> detections;
    vector<FrameDetection> blobs;
    Mat image, drawing,BW,BW_green;
    int dilation = 3, erosion=1;
    vector<vector<Point> >  valid_contours, blob_contours;
    double low_H_g=168, low_S_g=57, low_V_g=54, high_H_g=7, high_S_g=186, high_V_g=151;//GREEN
    double low_H_1=172, low_S_1=127, low_V_1=69, high_H_1=15, high_S_1=255, high_V_1=255;//GREEN
    double low_H_2=168, low_S_2=57, low_V_2=54, high_H_2=7, high_S_2=186, high_V_2=151;//GREEN
    double low_H_3=168, low_S_3=57, low_V_3=54, high_H_3=7, high_S_3=186, high_V_3=151;//GREEN
    double low_H_4=168, low_S_4=57, low_V_4=54, high_H_4=7, high_S_4=186, high_V_4=151;//GREEN
    double low_H_5=168, low_S_5=57, low_V_5=54, high_H_5=7, high_S_5=186, high_V_5=151;//GREEN
    double low_H_6=168, low_S_6=57, low_V_6=54, high_H_6=7, high_S_6=186, high_V_6=151;//GREEN
    double low_H_7=168, low_S_7=57, low_V_7=54, high_H_7=7, high_S_7=186, high_V_7=151;//GREEN
    double low_H_8=168, low_S_8=57, low_V_8=54, high_H_8=7, high_S_8=186, high_V_8=151;//GREEN
    // double low_H=12, low_S=80, low_V=150, high_H=20, high_S=255, high_V=255;//Orange
    // double low_H=5, low_S=40, low_V=171, high_H=40, high_S=255, high_V=255;//Orange
    int low_H=168, low_S=57, low_V=54, high_H=7, high_S=186, high_V=151;//GREEN
    double area_ratio_thresh=0.5, aspectratio_thresh=0.9, area_thresh=7000,area_blob_thresh=5000;
    bool isValid(vector<Point> contour);
    void frame2bw();
    void findSquareContours( );
public:
    FrameDetector(string configfile);
    FrameDetector();
    vector<FrameDetection> detectFrames(const Mat image)  ; // Perform the detection
    vector<FrameDetection> detectFrames(const Mat image, int frame_number)  ; // Perform the detection

    vector<FrameDetection> getLastDetections()  {return detections;} // returns the detections of the last image without recomputing anything
    vector<FrameDetection> getBlobs() { return blobs;} //Returns the blobs detected in the last image without recomputing anything. Each blob detection is composed of the four corners of the bounding box of an orange blob. This should be only used when we dont have  any gate detections.
    void setDebug(bool flag) {debug=flag;}              
    void setVisualization (bool flag) { show=flag;}
    Mat getDetection();


};
