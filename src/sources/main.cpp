#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include<vector>
#include<cstdlib>
#include <cmath>
#include <opencv2/imgproc/imgproc.hpp>
#include "FrameDetector.h"
//g++ main.cpp  FrameDetector.cpp FrameDetector.h `pkg-config --cflags --libs opencv`

using namespace cv;
using namespace std;


int main( int argc, char** argv )
{

VideoCapture cap("iros_complete_race_sim.mp4"); 
FrameDetector detector; 
  // Check if camera opened successfully
  if(!cap.isOpened()){
    cout << "Error opening video stream or file" << endl;
    return -1;
  }
     
  while(1){
 
    Mat frame;
    // Capture frame-by-frame
    cap >> frame;
    // If the frame is empty, break immediately
    if (frame.empty())
      break;
detector.detectFrames(frame);
  }
  
  cap.release();
 
  destroyAllWindows();
    return 0;
}