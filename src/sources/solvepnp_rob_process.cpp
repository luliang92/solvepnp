#include "solvepnp_rob_process.h"

solvepnp_rob_process::solvepnp_rob_process(string config_file):
    RobotProcess(),framedetector(config_file)
{
    std::cout << "solve_pnp constructor " << std::endl;
}

solvepnp_rob_process::~solvepnp_rob_process()
{
    std::cout << "solve_pnp destructor " << std::endl;
}


void solvepnp_rob_process::ownSetUp()
{
    //assign all the variables here
    //initializing the pnp solver
    this->init();

    // Initialize vairable
    next_gate_to_cross_ = 1;
}

void solvepnp_rob_process::ownStop()
{
    //assign all the variables to default here
}

void solvepnp_rob_process::ownRun()
{
    //run the algorithm over here

}

void solvepnp_rob_process::init()
{

    ros::param::get("~stack_path_", stack_path_);
    if(stack_path_.length() == 0)
        stack_path_ = "~/workspace/ros/aerostack_catkin_ws/src/aerostack_stack";

    ros::param::get("~drone_id_int", drone_id_);
    if(drone_id_.length() == 0)
    {
      drone_id_="7";
    }

    ros::param::get("~drone_id_namespace", drone_id_namespace);
    if(drone_id_namespace.length() == 0)
    {
      drone_id_namespace="drone7";
    }

    ros::param::get("~image_receiver_topic_name",image_receiver_topic_name);
    if(image_receiver_topic_name.length() == 0)
    {
      image_receiver_topic_name="/hummingbird_adr1/camera_front/image_raw";
    }
    ros::param::get("~camera_info_topic_name",camera_info_topic_name);
    if(camera_info_topic_name.length() == 0)
    {
      camera_info_topic_name="/hummingbird_adr1/camera_front/camera_info";
    }
    ros::param::get("~pnp_result_topic_name",pnp_result_topic_name);
    if(pnp_result_topic_name.length() == 0)
    {
      pnp_result_topic_name="/Re_PNP";
    }
    ros::param::get("~frame_points_topic_name",frame_points_topic_name);
    if(frame_points_topic_name.length() == 0)
    {
      frame_points_topic_name="/frame_points";
    }

    ros::param::get("~blob_points_topic_name",blob_points_topic_name);
    if(blob_points_topic_name.length() == 0)
    {
      blob_points_topic_name="/blob_points";
    }


    ros::param::get("~detector_image_topic_name",detector_image_topic_name);
    if(detector_image_topic_name.length() == 0)
    {
      detector_image_topic_name="detector_img";
    }





}

void solvepnp_rob_process::ownStart()
{

    //start all the subscribers and publishers over here
    //pic_sub = n.subscribe("/hummingbird_adr1/camera_front/image_raw", 1, &solvepnp_rob_process::picCallback,this);
    //camera_para = n.subscribe("/hummingbird_adr1/camera_front/camera_info",1,&solvepnp_rob_process::rgbCamInfoCallback,this);
    image_transport::ImageTransport it(n);
    pic_sub = n.subscribe(image_receiver_topic_name, 1, &solvepnp_rob_process::picCallback,this);
    camera_para = n.subscribe(camera_info_topic_name,1,&solvepnp_rob_process::rgbCamInfoCallback,this);
    next_gate_sub = n.subscribe("/next_gate_to_cross",1,&solvepnp_rob_process::nextGateToCrossCallback,this);
    //ros::Subscriber aruco_detection = n.subscribe("/aruco_eye/aruco_observation",1, arucoDetectionCallback);

    Re2pnp =n.advertise<solvepnp::FramePoses>(pnp_result_topic_name,1);
    frame_points =n.advertise<solvepnp::FramePoints>(frame_points_topic_name,1);
    blob_points = n.advertise<solvepnp::FramePoints>(blob_points_topic_name,1);
    detector_image_pub = it.advertise(detector_image_topic_name,1);
}

void solvepnp_rob_process::rgbCamInfoCallback(const sensor_msgs::CameraInfo &msg)
{

    fx = msg.K.at(0);
    cx = msg.K.at(2);
    fy = msg.K.at(4);
    cy = msg.K.at(5);
    e1 = msg.D.at(0);
    e2 = msg.D.at(1);
    e3 = msg.D.at(2);
    e4 = msg.D.at(3);
    e5 = msg.D.at(4);

    return;

}

void solvepnp_rob_process::nextGateToCrossCallback(const std_msgs::Int32 &msg){
  next_gate_to_cross_ = msg.data;
}

void solvepnp_rob_process::picCallback(const sensor_msgs::ImageConstPtr& msg)
{
    //initial


    PNPSolver p4psolver;

    cv::Mat raw_img;

    cv::Mat detector_img;

    cv::Point2f Point_BOTTOM_LEFT;
    cv::Point2f Point_BOTTOM_RIGHT;
    cv::Point2f Point_UP_RIGHT;
    cv::Point2f Point_UP_LEFT;

    std::vector<FrameDetection> frameCallBack;
    std::vector<FrameDetection> frameBlobs;

    solvepnp::FramePoints FramePoints;
    solvepnp::FramePoints BlobPoints;
    solvepnp::FramePoses FramePoses;
    //-----------------------------
    raw_img = cv_bridge::toCvShare(msg, "bgr8")->image;
    frameCallBack = framedetector.detectFrames(raw_img,next_gate_to_cross_);
    frameBlobs = framedetector.getBlobs();
    detector_img = framedetector.getDetection();
    sensor_msgs::ImagePtr detector_img_out = cv_bridge::CvImage(std_msgs::Header(), "bgr8", detector_img).toImageMsg();
    detector_image_pub.publish(detector_img_out);

    if(!frameCallBack.empty())
    {
        std::vector<Eigen::RowVector3d> initial_3d_points(4);
        std::vector<Eigen::RowVector3d> rotated_3d_points(4);
        for(int i =0;i<frameCallBack.size();i++)
        {
//            std::cout<<"frame number ="<<frameCallBack.size()<<std::endl;
//            std::cout<<"frame callback =" << frameCallBack[i].getCorners() <<std::endl;
//            std::cout<<"loop i ="<<i<<std::endl;

            Point_BOTTOM_LEFT.x = frameCallBack[i].getCorners()[0].x;
            Point_BOTTOM_LEFT.y = frameCallBack[i].getCorners()[0].y;
            Point_BOTTOM_RIGHT.x = frameCallBack[i].getCorners()[1].x;
            Point_BOTTOM_RIGHT.y = frameCallBack[i].getCorners()[1].y;
            Point_UP_RIGHT.x = frameCallBack[i].getCorners()[2].x;
            Point_UP_RIGHT.y = frameCallBack[i].getCorners()[2].y;
            Point_UP_LEFT.x = frameCallBack[i].getCorners()[3].x;
            Point_UP_LEFT.y = frameCallBack[i].getCorners()[3].y;


            //---------------------------------------------------------------------------------------------------------------------//

            p4psolver.SetCameraMatrix(fx,fy,cx,cy);
            //init distortion param
            p4psolver.SetDistortionCoefficients(e1, e2, e3, e4, e5);

            //----------------------------------------------------------------------------------------------------------------//
            if(/*frameCallBack[i].getID() == "CLOSEDGATE"*/ (next_gate_to_cross_ < 6) || (next_gate_to_cross_ > 7))
            {
                FramePoses.frame_id = "CLOSEDGATE";
                p4psolver.Points3D.push_back(cv::Point3f(0, 0, 0));     //P1/m
                p4psolver.Points3D.push_back(cv::Point3f(1.4, 0, 0));   //P2/m
                p4psolver.Points3D.push_back(cv::Point3f(1.4, 1.4, 0));   //P3/m
                //p4psolver.Points3D.push_back(cv::Point3f(150, 200, 0));   //P4/m
                p4psolver.Points3D.push_back(cv::Point3f(0, 1.4, 0)); //P5/m
            }
            if(/*frameCallBack[i].getID() == "JUNGLEGYM"*/ next_gate_to_cross_ == 6) // FOR NOW WE DO NOT MAKE DIFFERENCE BETWEEN JUNGLE AND CLOSED
            {
                FramePoses.frame_id = "JUNGLEGYM";
                // p4psolver.Points3D.push_back(cv::Point3f(0, 0, 0));     //P1/m
                // p4psolver.Points3D.push_back(cv::Point3f(1.0, 0, 0));   //P2/m
                // p4psolver.Points3D.push_back(cv::Point3f(1.0, 1.0, 0));   //P3/m
                // //p4psolver.Points3D.push_back(cv::Point3f(150, 200, 0));   //P4/m
                // p4psolver.Points3D.push_back(cv::Point3f(0, 1.0, 0)); //P5/m



                p4psolver.Points3D.push_back(cv::Point3f(0, 0, 0));     //P1/m
                p4psolver.Points3D.push_back(cv::Point3f(1.4, 0, 0));   //P2/m
                p4psolver.Points3D.push_back(cv::Point3f(1.4, 1.4, 0));   //P3/m
                //p4psolver.Points3D.push_back(cv::Point3f(150, 200, 0));   //P4/m
                p4psolver.Points3D.push_back(cv::Point3f(0, 1.4, 0)); //P5/m
            }

            if(/*frameCallBack[i].getID() == "DYNAMICGATE"*/ next_gate_to_cross_ == 7)
            {
                FramePoses.frame_id = "DYNAMICGATE";
                p4psolver.Points3D.push_back(cv::Point3f(0, 0, 0));     //P1/m
                p4psolver.Points3D.push_back(cv::Point3f(2.0, 0, 0));   //P2/m
                p4psolver.Points3D.push_back(cv::Point3f(2.1, 2.1, 0));   //P3/m
                //p4psolver.Points3D.push_back(cv::Point3f(150, 200, 0));   //P4/m
                p4psolver.Points3D.push_back(cv::Point3f(0, 2.1, 0)); //P5/m
            }

            if(frameCallBack[i].getID() == "UNKNOWN")
            {
                FramePoses.frame_id = "UNKNOWN";
                p4psolver.Points3D.push_back(cv::Point3f(0, 0, 0));     //P1/m
                p4psolver.Points3D.push_back(cv::Point3f(1.4, 0 ,0));   //P2/m
                p4psolver.Points3D.push_back(cv::Point3f(1.4, 1.4, 0));   //P3/m
                //p4psolver.Points3D.push_back(cv::Point3f(150, 200, 0));   //P4/m
                p4psolver.Points3D.push_back(cv::Point3f(0, 1.4, 0)); //P5/m
            }

            //------------------------------------------------------------------------------------------------------------------------//
            //init feature point picture frame
            p4psolver.Points2D.push_back(Point_UP_LEFT);   //P4
            p4psolver.Points2D.push_back(Point_UP_RIGHT);  //P1
            p4psolver.Points2D.push_back(Point_BOTTOM_RIGHT);  //P2
            p4psolver.Points2D.push_back(Point_BOTTOM_LEFT);  //P3

//            std::cout<<  "Points3D_size ="<<p4psolver.Points3D.size()<<std::endl;
//            std::cout << "points 2D" << p4psolver.Points2D << std::endl;

            // -----------------------------------------------------------------3 methods for solvepnp--------------------------------//

            p4psolver.Solve(PNPSolver::METHOD::CV_ITERATIVE);
            cv::Mat rotation_vec, translation_vec;
            p4psolver.getRotationAndTranslation(rotation_vec, translation_vec);

            //            double x = 0, y = 0, z = 0, roll = 0, pitch = 0, yaw = 0;
            //            p4psolver.convertToAerostackFrame(x, y, z, roll, pitch, yaw, rotation_vec, translation_vec);

            //            tf::Quaternion quaternion = tf::createQuaternionFromRPY(roll, pitch, yaw);

            //---------------------------------------------------------------------Rotation to Quaternion-----------------------------//

            cv::Mat RoteM;
            cv::Rodrigues(rotation_vec, RoteM);

            Eigen::Matrix3d Rmat_E;
            Rmat_E(0,0) = RoteM.at<double>(0,0);
            Rmat_E(0,1) = RoteM.at<double>(0,1);
            Rmat_E(0,2) = RoteM.at<double>(0,2);

            Rmat_E(1,0) = RoteM.at<double>(1,0);
            Rmat_E(1,1) = RoteM.at<double>(1,1);
            Rmat_E(1,2) = RoteM.at<double>(1,2);

            Rmat_E(2,0) = RoteM.at<double>(2,0);
            Rmat_E(2,1) = RoteM.at<double>(2,1);
            Rmat_E(2,2) = RoteM.at<double>(2,2);

            if (i == 0)
            {
                initial_3d_points[0](0,0) = translation_vec.at<double>(0,0);
                initial_3d_points[0](0,1) = translation_vec.at<double>(1,0)+1.4;
                initial_3d_points[0](0,2) = translation_vec.at<double>(2,0);

                initial_3d_points[1](0,0) = translation_vec.at<double>(0,0)+1.4;
                initial_3d_points[1](0,1) = translation_vec.at<double>(1,0)+1.4;
                initial_3d_points[1](0,2) = translation_vec.at<double>(2,0);

                initial_3d_points[2](0,0) = translation_vec.at<double>(0,0)+1.4;
                initial_3d_points[2](0,1) = translation_vec.at<double>(1,0);
                initial_3d_points[2](0,2) = translation_vec.at<double>(2,0);

                initial_3d_points[3](0,0) = translation_vec.at<double>(0,0);
                initial_3d_points[3](0,1) = translation_vec.at<double>(1,0);
                initial_3d_points[3](0,2) = translation_vec.at<double>(2,0);

                rotated_3d_points[0] = initial_3d_points[0] * Rmat_E;
                rotated_3d_points[1] = initial_3d_points[1] * Rmat_E;
                rotated_3d_points[2] = initial_3d_points[2] * Rmat_E;
                rotated_3d_points[3] = initial_3d_points[3] * Rmat_E;
            }
            Eigen::Quaterniond quaternion(Rmat_E);
            tf::Quaternion quaterniond;
            tf::quaternionEigenToTF(quaternion,quaterniond);

            tf::Matrix3x3 m(quaterniond);

            //convert quaternion to euler angels
            double yaw, pitch, roll;
            m.getEulerYPR(yaw, pitch, roll);

//            std::cout << "roll: " << roll
//                      << "pitch: " << pitch
//                      << "yaw: "  << yaw << std::endl;

//            std::cout <<  "trans: " << translation_vec << std::endl;

            //publish
            geometry_msgs::PoseStamped Re_PNP;
            Re_PNP.header.frame_id = frameCallBack[i].getID();
            Re_PNP.header.stamp    = ros::Time::now();
            Re_PNP.pose.position.x = translation_vec.at<double>(0,0);
            Re_PNP.pose.position.y = translation_vec.at<double>(1,0);
            Re_PNP.pose.position.z = translation_vec.at<double>(2,0);
            Re_PNP.pose.orientation.w = quaternion.w();
            Re_PNP.pose.orientation.x = quaternion.x();
            Re_PNP.pose.orientation.y = quaternion.y();
            Re_PNP.pose.orientation.z = quaternion.z();
            FramePoses.poses.push_back(Re_PNP);

            p4psolver.Points3D.clear();
            p4psolver.Points2D.clear();
        }

        //-----------------------------------------------------publish_frame---------------------------------------------------//

        //frame_points.publish(FramePoints);
        geometry_msgs::PointStamped PointsInFrame;
        //this first frame received is the biggest frame and that should be published only
        for(int j = 0; j<4; j++)
        {
            PointsInFrame.header.frame_id = "cam_frame";
            PointsInFrame.header.stamp    = ros::Time::now();
            PointsInFrame.point.x = frameCallBack[0].getCorners()[j].x;
            PointsInFrame.point.y = frameCallBack[0].getCorners()[j].y;
            PointsInFrame.point.z = rotated_3d_points[j](0,2);
            FramePoints.points.push_back(PointsInFrame);
        }
    }
    else
    {

        std::cout<<"no frame in the camera image!"<<std::endl;
        geometry_msgs::PointStamped Blob_Points;

        if(!frameBlobs.empty())
         {

            for(int j = 0; j<4; j++)
            {
                Blob_Points.header.frame_id = "cam_frame";
                Blob_Points.header.stamp    = ros::Time::now();
                Blob_Points.point.x = frameBlobs[0].getCorners()[j].x;
                Blob_Points.point.y = frameBlobs[0].getCorners()[j].y;
                Blob_Points.point.z = 0;
                BlobPoints.points.push_back(Blob_Points);
            }

        }
        else
            {
            std::cout<<"no frame blobs in the camera image"<<std::endl;
        }

    }

    if(!BlobPoints.points.empty())
    {
        blob_points.publish(BlobPoints);
        BlobPoints.points.empty();
    }
    if(!FramePoints.points.empty())
    {
        frame_points.publish(FramePoints);
        FramePoints.points.clear();
    }
    if(!FramePoses.poses.empty())
    {
        Re2pnp.publish(FramePoses);
    }

}
