#include "PNPsolver.h"


// This class is used to solve pnp problem
//
PNPSolver::PNPSolver()
{
    //initial output matrix
    vector<double> rv(3), tv(3);
    cv::Mat rvec(rv), tvec(tv);
}
PNPSolver::PNPSolver(double fx, double fy, double u0, double v0, double k_1, double  k_2, double  p_1, double  p_2, double k_3)
{
    //initial output matrix
    vector<double> rv(3), tv(3);
    cv::Mat rvec(rv), tvec(tv);
    SetCameraMatrix(fx, fy, u0, v0);
    SetDistortionCoefficients(k_1, k_2, p_1, p_2, k_3);
}

PNPSolver::~PNPSolver()
{
}

int PNPSolver::Solve(METHOD method)
{
    //data test
    if (camera_matrix.cols == 0 || distortion_coefficients.cols == 0)
    {
        printf("ErrCode:-1,the parameter of camera not set\r\n");
        return -1;
    }

    if (Points3D.size() != Points2D.size())
    {
        printf("ErrCode:-2，the numbers of 2D points and 3D points are not the same！\r\n");
        return -2;
    }
    if (method == METHOD::CV_P3P || method == METHOD::CV_ITERATIVE)
    {
        if (Points3D.size() != 4)
        {
            printf("ErrCode:-2,the numbers of input feature points shouldbe 4 when using CV_ITERATIVE or CV_P3P！\r\n");
            return -2;
        }
    }
    else
    {
        if (Points3D.size() < 4)
        {
            printf("ErrCode:-2,the numbers of input feature points should be larger than 4！\r\n");
            return -2;
        }
    }

    ////TODO::test if the feature points are in the same plane
    //if ((method == METHOD::CV_ITERATIVE || method == METHOD::CV_EPNP) && Points2D.size() == 4)
    //{
    //
    //}






    /*******************solve PNP problem*********************/
    //3 ways to solve problem

    //cv::solvePnPRansac(Points3D, Points2D, camera_matrix, distortion_coefficients, rvec, tvec, false, iterationsCount,reprojectionError,confidence,inliers,method);
    solvePnP(Points3D, Points2D, camera_matrix, distortion_coefficients, rvec, tvec);

    //solvePnP(Points3D, Points2D, camera_matrix, distortion_coefficients, rvec, tvec, false, CV_P3P);
    //solvePnP(Points3D, Points2D, camera_matrix, distortion_coefficients, rvec, tvec, false, CV_EPNP);

    //std::cout << "first rvec" << rvec << std::endl;

    /*******************get rotation*********************/
    //    double rm[9];
    //    RoteM = cv::Mat(3, 3, CV_64FC1, rm);
    //    //cout<<"rotation = "<<rvec<<endl;
    //    cv::Rodrigues(rvec, RoteM);
    //    //    double r11 = RoteM.ptr<double>(0)[0];
    //    //    double r12 = RoteM.ptr<double>(0)[1];
    //    //    double r13 = RoteM.ptr<double>(0)[2];
    //    //    double r21 = RoteM.ptr<double>(1)[0];
    //    //    double r22 = RoteM.ptr<double>(1)[1];
    //    //    double r23 = RoteM.ptr<double>(1)[2];
    //    //    double r31 = RoteM.ptr<double>(2)[0];
    //    //    double r32 = RoteM.ptr<double>(2)[1];
    //    //    double r33 = RoteM.ptr<double>(2)[2];
    //    TransM = tvec;


    return 0;
}



vector<cv::Point2f> PNPSolver::WordFrame2ImageFrame(vector<cv::Point3f> WorldPoints)
{
    vector<cv::Point2f> projectedPoints;
    cv::projectPoints(WorldPoints, rvec, tvec, camera_matrix, distortion_coefficients, projectedPoints);
    return projectedPoints;
}




cv::Point3f PNPSolver::ImageFrame2CameraFrame(cv::Point2f p, double F)
{
    double fx;
    double fy;
    double u0;
    double v0;

    fx = camera_matrix.ptr<double>(0)[0];
    u0 = camera_matrix.ptr<double>(0)[2];
    fy = camera_matrix.ptr<double>(1)[1];
    v0 = camera_matrix.ptr<double>(1)[2];
    double zc = F;
    double xc = (p.x - u0)*F / fx;
    double yc = (p.y - v0)*F / fy;
    return cv::Point3f(xc, yc, zc);
}

void PNPSolver::convertToAerostackFrame(double &x, double &y, double &z, double &roll, double &pitch,
                                        double &yaw, cv::Mat rvec, cv::Mat tvec)
{
    //converting the the RVEC and TVEC to the one required by the SLAM lib of aerostack
    cv::Mat Rvec, Tvec;
    rvec.convertTo(Rvec, CV_32F);
    tvec.convertTo(Tvec, CV_32F);

    //this rotation is applied as per the aruco lib
    this->rotateXAxis(Rvec);

    cv::Mat matHomog_aruco_GMR_wrt_aruco_ALF;
    cv::Mat matHomog_drone_ALF_wrt_drone_GMR;

    double x1, y1, z1, yaw1, pitch1, roll1;
    x1 = 0.2, y1 = 0.0, z1=0.035, yaw1 = -90*(M_PI/180), pitch1 = 0, roll1 = -90*(M_PI/180);

    matHomog_drone_ALF_wrt_drone_GMR = cv::Mat::eye(4,4,CV_32F);
    referenceFrames::createHomogMatrix_wYvPuR( &matHomog_drone_ALF_wrt_drone_GMR, x1, y1, z1, yaw1, pitch1, roll1);

    // matHomog_aruco_GMR_wrt_aruco_ALF
    matHomog_aruco_GMR_wrt_aruco_ALF = cv::Mat::eye(4,4,CV_32F);

    // assign value to matHomog_aruco_GMR_wrt_aruco_ALF
    double x2 = 0.0, y2 = 0.0, z2 = 0.0;
    double yaw2    = (M_PI/180.0)*(+90.0); // rad
    double pitch2 = 0.0;
    double roll2   = (M_PI/180.0)*(-90.0); // rad
    referenceFrames::createHomogMatrix_wYvPuR( &matHomog_aruco_GMR_wrt_aruco_ALF, x2, y2, z2, yaw2, pitch2, roll2);


    cv::Mat matHomog_aruco_GMR_wrt_drone_GMR(4,4,CV_32F);       // as required by ArUCo SLAM module
    cv::Mat matHomog_aruco_ALF_wrt_drone_ALF(4,4,CV_32F);       // as returned by the ArUCo library

    //Transform to center of gravity of the drone
    PNPSolver::createMatHomogFromVecs( matHomog_aruco_ALF_wrt_drone_ALF,  Tvec, Rvec);

    matHomog_aruco_GMR_wrt_drone_GMR = matHomog_drone_ALF_wrt_drone_GMR*
            matHomog_aruco_ALF_wrt_drone_ALF*
            matHomog_aruco_GMR_wrt_aruco_ALF;

    //double x = 0.0, y = 0.0, z = 0.0, yaw = 0.0, pitch = 0.0, roll = 0.0;
    referenceFrames::getxyzYPRfromHomogMatrix_wYvPuR( matHomog_aruco_GMR_wrt_drone_GMR, &x, &y, &z, &yaw, &pitch, &roll);

    std::cout << "X: " << x << "Y: " << y << "Z:" << z << "yaw: " << yaw << "pitch: " << pitch << "roll: " << roll << std::endl;

}

void PNPSolver::rotateXAxis(cv::Mat &rotation)
{
    cv::Mat R(3, 3, CV_32F);
    Rodrigues(rotation, R);
    // create a rotation matrix for x axis
    cv::Mat RX = cv::Mat::eye(3, 3, CV_32F);
    float angleRad = M_PI / 2;
    RX.at< float >(1, 1) = cos(angleRad);
    RX.at< float >(1, 2) = -sin(angleRad);
    RX.at< float >(2, 1) = sin(angleRad);
    RX.at< float >(2, 2) = cos(angleRad);
    // now multiply
    R = R * RX;
    // finally, the the rodrigues back
    Rodrigues(R, rotation);
}

int PNPSolver::createMatHomogFromVecs(cv::Mat& MatHomog, cv::Mat TransVec, cv::Mat RotVec)
{
    //MatHomog.create(Size(4,4),CV_64F);
    MatHomog=cv::Mat::zeros(4,4,CV_32F);


    //
    cv::Mat RotMat;
    cv::Rodrigues(RotVec,RotMat);

    //cout<<RotMat<<endl;


    //Rotations
    /*
    float roll=RotVec.at<float>(0,0);
    Mat MatHRoll=Mat::zeros(4,4,CV_32F);
    MatHRoll.at<float>(0,0)=1.0;
    MatHRoll.at<float>(1,1)=cos(roll); MatHRoll.at<float>(1,2)=-sin(roll);
    MatHRoll.at<float>(2,1)=sin(roll); MatHRoll.at<float>(2,2)=cos(roll);
    MatHRoll.at<float>(3,3)=1.0;
    float pitch=RotVec.at<float>(1,0);
    Mat MatHPitch=Mat::zeros(4,4,CV_32F);
    MatHPitch.at<float>(0,0)=cos(pitch); MatHPitch.at<float>(0,2)=sin(pitch);
    MatHPitch.at<float>(1,1)=1.0;
    MatHPitch.at<float>(2,0)=-sin(pitch); MatHPitch.at<float>(2,2)=cos(pitch);
    MatHPitch.at<float>(3,3)=1.0;
    float yaw=RotVec.at<float>(2,0);
    Mat MatHYaw=Mat::zeros(4,4,CV_32F);
    MatHYaw.at<float>(0,0)=cos(yaw); MatHYaw.at<float>(0,1)=-sin(yaw);
    MatHYaw.at<float>(1,0)=sin(yaw); MatHYaw.at<float>(1,1)=cos(yaw);
    MatHYaw.at<float>(2,2)=1.0;
    MatHPitch.at<float>(3,3)=1.0;
    *MatHomog=MatHRoll*MatHPitch*MatHYaw;
    */
    for(int i=0;i<3;i++)
        for(int j=0;j<3;j++)
            MatHomog.at<float>(i,j)=RotMat.at<float>(i,j);



    //Translation
    for(int i=0;i<3;i++)
    {
        MatHomog.at<float>(i,3)=TransVec.at<float>(i,0);
    }


    //Scale
    MatHomog.at<float>(3,3)=1.0;


    //cout<<*MatHomog<<endl;

    return 1;
}
