#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include<vector>
#include <cmath>
#include <opencv2/imgproc/imgproc.hpp>
#include<cstdlib>
#include "FrameDetector.h"
using namespace std;
using namespace cv;

void trackbar_func ( int position , void * userdata){

    *((int*)&userdata) = position;
}

    FrameDetector::FrameDetector(){ 
    cout<<"DETECTOR CREADO"<<endl;
        
        
    show = true;
    namedWindow("Configuration window");
    createTrackbar("Low H", "Configuration window", &low_H, 180,  trackbar_func , &low_H);
    createTrackbar("High H", "Configuration window", &high_H, 180,  trackbar_func, &high_H);
    createTrackbar("Low S", "Configuration window", &low_S, 255,  trackbar_func, &low_S);
    createTrackbar("High S", "Configuration window", &high_S, 255,  trackbar_func, &high_S);
    createTrackbar("Low V", "Configuration window", &low_V, 255,  trackbar_func, &low_V);
    createTrackbar("High V", "Configuration window", &high_V, 255,  trackbar_func, &high_V);
    createTrackbar("Dilation", "Configuration window", &dilation, 10,  trackbar_func, &dilation);
    createTrackbar("Erosion", "Configuration window", &erosion, 10,  trackbar_func, &erosion);

    }



 #include "xmlfilereader.h"

 FrameDetector::FrameDetector(string configfile){

      XMLFileReader my_xml_reader(configfile);
     // DEbugging
     debug = my_xml_reader.readDoubleValue("FrameDetector:Debugging:Debug");
     show= my_xml_reader.readDoubleValue("FrameDetector:Debugging:Visualization");
     //binarization thresholds
     low_H = my_xml_reader.readDoubleValue("FrameDetector:GateBinarization:Hue:Low");
     low_S = my_xml_reader.readDoubleValue("FrameDetector:GateBinarization:Saturation:Low");
     low_V = my_xml_reader.readDoubleValue("FrameDetector:GateBinarization:Value:Low");    
     high_H = my_xml_reader.readDoubleValue("FrameDetector:GateBinarization:Hue:High");
     high_S = my_xml_reader.readDoubleValue("FrameDetector:GateBinarization:Saturation:High");
     high_V = my_xml_reader.readDoubleValue("FrameDetector:GateBinarization:Value:High");
     //Contour properties
     area_ratio_thresh = my_xml_reader.readDoubleValue("FrameDetector:GateProperties:AreaRatio");
     aspectratio_thresh = my_xml_reader.readDoubleValue("FrameDetector:GateProperties:AspectRatioDeviation");
     area_thresh= my_xml_reader.readDoubleValue("FrameDetector:GateProperties:MinimumArea");
     area_blob_thresh= my_xml_reader.readDoubleValue("FrameDetector:BlobProperties:MinimumArea");
    if(this->show){
     namedWindow("Configuration window");
    createTrackbar("Low H", "Configuration window", &low_H, 180,  trackbar_func , &low_H);
    createTrackbar("High H", "Configuration window", &high_H, 180,  trackbar_func, &high_H);
    createTrackbar("Low S", "Configuration window", &low_S, 255,  trackbar_func, &low_S);
    createTrackbar("High S", "Configuration window", &high_S, 255,  trackbar_func, &high_S);
    createTrackbar("Low V", "Configuration window", &low_V, 255,  trackbar_func, &low_V);
    createTrackbar("High V", "Configuration window", &high_V, 255,  trackbar_func, &high_V);
    createTrackbar("Dilation", "Configuration window", &dilation, 10,  trackbar_func, &dilation);
    createTrackbar("Erosion", "Configuration window", &erosion, 10,  trackbar_func, &erosion);
   }
}





    vector<FrameDetection> FrameDetector::detectFrames(const Mat image, int gate_number)  {
        if(!show){
        switch(gate_number){
            case 1:
            low_H=low_H_1;
            low_S=low_S_1;
            low_V=low_V_1;
            high_H=high_V_1;
            high_S=high_S_1;
            high_V=high_V_1;
            break;
            case 2:
            low_H=low_H_2;
            low_S=low_S_2;
            low_V=low_V_2;
            high_H=high_V_2;
            high_S=high_S_2;
            high_V=high_V_2;
            break;
            case 3:
            low_H=low_H_3;
            low_S=low_S_3;
            low_V=low_V_3;
            high_H=high_V_3;
            high_S=high_S_3;
            high_V=high_V_3;
            break;
            case 4:
            low_H=low_H_4;
            low_S=low_S_4;
            low_V=low_V_4;
            high_H=high_V_4;
            high_S=high_S_4;
            high_V=high_V_4;
            break;
            case 5:
            low_H=low_H_5;
            low_S=low_S_5;
            low_V=low_V_5;
            high_H=high_V_5;
            high_S=high_S_5;
            high_V=high_V_5;
            break;
            case 6:
            low_H=low_H_6;
            low_S=low_S_6;
            low_V=low_V_6;
            high_H=high_V_6;
            high_S=high_S_6;
            high_V=high_V_6;
            break;
            case 7:
            low_H=low_H_7;
            low_S=low_S_7;
            low_V=low_V_7;
            high_H=high_V_7;
            high_S=high_S_7;
            high_V=high_V_7;
            break;
            case 8:
            low_H=low_H_8;
            low_S=low_S_8;
            low_V=low_V_8;
            high_H=high_V_8;
            high_S=high_S_8;
            high_V=high_V_8;
            break;

        }
        }
        return this->detectFrames(image); 

    }


Mat equalizeIntensity( Mat& inputImage)
{
    if(inputImage.channels() >= 3)
    {
        Mat ycrcb;

        cvtColor(inputImage,ycrcb,CV_BGR2YCrCb);

        vector<Mat> channels;
        split(ycrcb,channels);

        equalizeHist(channels[0], channels[0]);

        Mat result;
        merge(channels,ycrcb);

        cvtColor(ycrcb,result,CV_YCrCb2BGR);

        return result;
    }
    return Mat();
}

void FrameDetector::frame2bw(){
    Mat frame_HSV;
    // image=equalizeIntensity(image);

    cvtColor(image, frame_HSV, COLOR_BGR2HSV);
    if(low_H<high_H) inRange(frame_HSV, Scalar(low_H, low_S, low_V), Scalar(high_H, high_S, high_V), BW);
    else {
        Mat BW1,BW2;
        inRange(frame_HSV, Scalar(0, low_S, low_V), Scalar(high_H, high_S, high_V), BW1);
        inRange(frame_HSV, Scalar(low_H, low_S, low_V), Scalar(180, high_S, high_V), BW2);
        BW=BW1+ BW2;
    }
    inRange(frame_HSV, Scalar(low_H_g, low_S_g, low_V_g), Scalar(high_H_g, high_S_g, high_V_g), BW_green);
    if(dilation)
dilate(BW, BW, Mat(), Point(-1, -1), dilation, 1, 1);
if(erosion)
erode(BW, BW, Mat(), Point(-1, -1), erosion, 1, 1);

    // return frame_threshold;_g
}

bool FrameDetector::isValid(vector<Point> contour){
    Rect box = boundingRect(contour);
    double aspect_ratio=double(box.height)/double(box.width);
    double contour_area=contourArea(contour);
    double bb_area=double(box.height)*double(box.width);
    bool flag= abs(1-aspect_ratio) < aspectratio_thresh  && contour_area > area_thresh  && !isContourConvex( contour);// && isContourConvex(contour) ;//&& (1-(bb_area/contour_area) )< area_ratio_thresh;//&& bb_area/contour_area < area_ratio_thresh ;
    return flag;
}

void FrameDetector::findSquareContours( ){
    valid_contours.clear();
    blob_contours.clear();
    hasBase.clear();
    vector<vector<Point> > contours;
    Mat contourOutput = BW.clone();
    vector<Vec4i> hierarchy;
    findContours( contourOutput, contours,hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE );

    for( int i = 0; i< contours.size(); i++ ){
        vector<Point> contour = contours[i];
        if(hierarchy[i][3] !=-1){
            if(isValid(contour)){
                valid_contours.push_back(contour);
                vector<Point> par = contours[hierarchy[i][3]];
                Rect box = boundingRect(par);
                double aspect_ratio=double(box.height)/double(box.width);
                double bb_area=double(box.height)*double(box.width);
                bool flag  = abs(aspect_ratio - 1) > 0.1;
                // cout<<abs(aspect_ratio - 1)<<endl;
                // cout<<(flag? "CLOSED": "JUNGLE") <<flag<<endl;
                hasBase.push_back(flag);
            }}
        else{
               if(contourArea(contour) > area_blob_thresh) blob_contours.push_back(contour);
        }
    }
}


bool compareContours(vector<Point> a , vector<Point> b){
    Rect bb1 = boundingRect(a);
    Rect bb2 = boundingRect(b);
    return bb1.height*bb1.width > bb2.height*bb2.width;
}
bool compareContours2(std::pair< vector<Point>, bool >a , std::pair< vector<Point>, bool > b){
    Rect bb1 = boundingRect(a.first);
    Rect bb2 = boundingRect(b.first);
    return bb1.height*bb1.width > bb2.height*bb2.width;
}

vector<FrameDetection> FrameDetector::detectFrames(const Mat image)  {
    this->image = image.clone();
    this->frame2bw();
    this->findSquareContours( );



   std::vector<std::pair< vector<Point>, bool >>target;
target.reserve(valid_contours.size());
std::transform(valid_contours.begin(), valid_contours.end(), hasBase.begin(), std::back_inserter(target),[](vector<Point> a, bool b) { return std::make_pair(a, b); });

    sort(target.begin(), target.end(), compareContours2);
    sort(blob_contours.begin(), blob_contours.end(), compareContours);
    detections.clear();
    blobs.clear();
    for ( int i = 0 ; i<valid_contours.size() ; i++){
        FrameDetection d=FrameDetection();
        double epsilon = tolerance*arcLength(target[i].first,true);
        vector<Point> approx, approxOrdered,hull;
        approxPolyDP(target[i].first,approx,epsilon,true);
        if(approx.size() ==4){

            approxOrdered=approx;
            //SORT CORNERS
            Point center = (approx[0] + approx[1] + approx[2] + approx[3]) * 0.25 ;
            for(int j = 0 ; j<4; j++){
                Point v = approx[j] - center;
                if(v.x < 0 ){
                    if(v.y>0){
                        approxOrdered[0] = approx[j];
                    }
                    else{
                        approxOrdered[3] = approx[j];
                    }
                }
                else{
                    if(v.y<0){
                        approxOrdered[2] = approx[j];
                    }
                    else{
                        approxOrdered[1] = approx[j];
                    }
                }
            }
            d.setCorner(approxOrdered[0], 0);
            d.setCorner(approxOrdered[1], 1);
            d.setCorner(approxOrdered[2], 2);
            d.setCorner(approxOrdered[3], 3);
            if(target[i].second){
            d.setLabel("CLOSEDGATE");
                                        // cout<<"CLOSED CREATED"<<endl;

            }
            else{
                            d.setLabel("JUNGLEGYM");
                            // cout<<"JUNGLE CREATED"<<endl;

            }
            detections.push_back(d);
        }
        else{
                    convexHull(valid_contours[i],hull);
                    double epsilon = tolerance*arcLength(hull,true);
                    
                    approxPolyDP(hull,approx,epsilon,true);
                    if(approx.size() ==4){

            approxOrdered=approx;
            //SORT CORNERS
            Point center = (approx[0] + approx[1] + approx[2] + approx[3]) * 0.25 ;
            for(int j = 0 ; j<4; j++){
                Point v = approx[j] - center;
                if(v.x < 0 ){
                    if(v.y>0){
                        approxOrdered[0] = approx[j];
                    }
                    else{
                        approxOrdered[3] = approx[j];
                    }
                }
                else{
                    if(v.y<0){
                        approxOrdered[2] = approx[j];
                    }
                    else{
                        approxOrdered[1] = approx[j];
                    }
                }
            }
            d.setCorner(approxOrdered[0], 0);
            d.setCorner(approxOrdered[1], 1);
            d.setCorner(approxOrdered[2], 2);
            d.setCorner(approxOrdered[3], 3);
            d.setLabel("DYNAMICGATE");

            detections.push_back(d);
        }

        }
    }

    for ( int i = 0 ; i<blob_contours.size() ; i++){
            Rect bb = boundingRect(blob_contours[i]);
            FrameDetection d;
            d.setLabel("BLOB");
            d.setCorner(Point(bb.x , bb.y + bb.height), 0);
            d.setCorner(Point(bb.x + bb.width , bb.y + bb.height), 1);
            d.setCorner(Point(bb.x + bb.width , bb.y), 2);
            d.setCorner(Point(bb.x , bb.y), 3);
            blobs.push_back(d);
        }
    if(debug){
        vector<Vec4i> hierarchy;

        drawing = image;//Mat::zeros( BW.size(), CV_8UC3 );
        for( int i = 0; i< valid_contours.size(); i++ )
        {
            Scalar color = Scalar( 0, 255,0 );
            drawContours( drawing, valid_contours, i, color, 2, 8, hierarchy, 0, Point() );

        }
        for( int i = 0; i< blob_contours.size(); i++ )
        {
            Scalar color = Scalar( 0, 0,255);
            drawContours( drawing, blob_contours, i, color, 1, 8, hierarchy, 0, Point() );

        }
        for(int i=0; i < detections.size(); ++i)
        {  
            Scalar color; 
        if(detections[i].getID() == "CLOSEDGATE"){

             color = Scalar( 255, 0,0 );
        }
        else{
            if(detections[i].getID() == "DYNAMICGATE"){

             color = Scalar( 255, 0, 255 );
            }
            else{
                          if(detections[i].getID() == "JUNGLEGYM"){

             color = Scalar( 0, 255, 255 );
            }  
            }
        }
            for(int j =0 ; j<4; j++)
            {
                circle(drawing, detections[i].getCorner(j),5,color,5);
                circle(drawing, detections[i].getCorner(j),4,color,5);

            }
        }

       if (this->show){


       namedWindow( "Display window", WINDOW_AUTOSIZE );// Create a window for display.
       imshow( "Display window", drawing );

    //    namedWindow( "Configuration window", WINDOW_AUTOSIZE );// Create a window for display.
       imshow( "Configuration window", BW );
       waitKey(100);
       }
    }
    return detections;

}

cv::Mat FrameDetector::getDetection()
{
    return drawing;
}
