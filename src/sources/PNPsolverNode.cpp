//I/O Stream
//std::cout
#include <iostream>
#include <string>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <geometry_msgs/PointStamped.h>
#include <cv_bridge/cv_bridge.h>
#include <tf/tf.h>
#include <tf_conversions/tf_eigen.h>

// ROS
//ros::init(), ros::NodeHandle, ros::ok(), ros::spinOnce()
#include "ros/ros.h"
#include "PNPsolver.h"
#include "FrameDetector.h"

//msg
#include "solvepnp/FramePoints.h"
#include "solvepnp/FramePoses.h"

#include "aruco_eye_msgs/MarkerList.h"

using namespace std;

ros::Publisher frame_points;
ros::Publisher Re2pnp;

double fx;
double cx;
double fy;
double cy;
double e1;
double e2;
double e3;
double e4;
double e5;


void rgbCamInfoCallback(const sensor_msgs::CameraInfo &msg)
{

    fx = msg.K.at(0);
    cx = msg.K.at(2);
    fy = msg.K.at(4);
    cy = msg.K.at(5);
    e1 = msg.D.at(0);
    e2 = msg.D.at(1);
    e3 = msg.D.at(2);
    e4 = msg.D.at(3);
    e5 = msg.D.at(4);

    return;
}

//void arucoDetectionCallback(const aruco_eye_msgs::MarkerList& msg)
//{
//    PNPSolver p4psolver;
//    for(int i = 0; i < msg.markers.size(); ++i)
//    {
//        cv::Point2f Point_BOTTOM_LEFT;
//        cv::Point2f Point_BOTTOM_RIGHT;
//        cv::Point2f Point_UP_RIGHT;
//        cv::Point2f Point_UP_LEFT;

//        Point_UP_LEFT.x = msg.markers[i].pointsInImage[0].x;
//        Point_UP_LEFT.y = msg.markers[i].pointsInImage[0].y;

//        Point_UP_RIGHT.x = msg.markers[i].pointsInImage[1].x;
//        Point_UP_RIGHT.y = msg.markers[i].pointsInImage[1].y;

//        Point_BOTTOM_RIGHT.x = msg.markers[i].pointsInImage[2].x;
//        Point_BOTTOM_RIGHT.y = msg.markers[i].pointsInImage[2].y;

//        Point_BOTTOM_LEFT.x = msg.markers[i].pointsInImage[3].x;
//        Point_BOTTOM_LEFT.y = msg.markers[i].pointsInImage[3].y;


//        p4psolver.Points3D.push_back(cv::Point3f(0, 0, 0));     //P1/mm
//        p4psolver.Points3D.push_back(cv::Point3f(0.25, 0, 0));   //P2/mm
//        p4psolver.Points3D.push_back(cv::Point3f(0.25, 0.25, 0));   //P3/mm
//        p4psolver.Points3D.push_back(cv::Point3f(0, 0.25, 0)); //P5/mm

//        p4psolver.Points2D.push_back(Point_UP_LEFT);   //P4
//        p4psolver.Points2D.push_back(Point_UP_RIGHT);  //P1
//        p4psolver.Points2D.push_back(Point_BOTTOM_RIGHT);  //P2
//        p4psolver.Points2D.push_back(Point_BOTTOM_LEFT);  //P3

//        p4psolver.SetCameraMatrix(fx,fy,cx,cy);
//        p4psolver.SetDistortionCoefficients(e1, e2, e3, e4, e5);

//        p4psolver.Solve(PNPSolver::METHOD::CV_ITERATIVE);

//        cv::Mat rotation_vec, translation_vec;
//        p4psolver.getRotationAndTranslation(rotation_vec, translation_vec);

//        cv::Mat RoteM;
//        cv::Rodrigues(rotation_vec, RoteM);

//        Eigen::Matrix3d Rmat_E;
//        Rmat_E(0,0) = RoteM.at<double>(0,0);
//        Rmat_E(0,1) = RoteM.at<double>(0,1);
//        Rmat_E(0,2) = RoteM.at<double>(0,2);

//        Rmat_E(1,0) = RoteM.at<double>(1,0);
//        Rmat_E(1,1) = RoteM.at<double>(1,1);
//        Rmat_E(1,2) = RoteM.at<double>(1,2);

//        Rmat_E(2,0) = RoteM.at<double>(2,0);
//        Rmat_E(2,1) = RoteM.at<double>(2,1);
//        Rmat_E(2,2) = RoteM.at<double>(2,2);

//        Eigen::Quaterniond quaternion(Rmat_E);
//        tf::Quaternion quaterniond;
//        tf::quaternionEigenToTF(quaternion,quaterniond);

//        tf::Matrix3x3 m(quaterniond);

//        //convert quaternion to euler angels
//        double yaw, pitch, roll;
//        m.getEulerYPR(yaw, pitch, roll);

//        std::cout << "roll: " << roll
//                  << "pitch: " << pitch
//                  << "yaw: "  << yaw << std::endl;

//        std::cout <<  "trans: " << translation_vec << std::endl;

//    }

//}


void picCallback(const sensor_msgs::ImageConstPtr& msg)
{
    //initial
    cv::Mat raw_img;

    cv::Point2f Point_BOTTOM_LEFT;
    cv::Point2f Point_BOTTOM_RIGHT;
    cv::Point2f Point_UP_RIGHT;
    cv::Point2f Point_UP_LEFT;
    //  cv::Point3f Position_OcInW;
    //  cv::Point3f Theta_W2C;
    tf::Matrix3x3 Rect;
    cv::Mat trans;
    cv::Mat rotation_mat;
    std::vector<FrameDetection> frameCallBack;
    FrameDetector framedetector;
    PNPSolver p4psolver;
    solvepnp::FramePoints FramePoints;
    solvepnp::FramePoses FramePoses;
    //-----------------------------
    raw_img = cv_bridge::toCvShare(msg, "bgr8")->image;
    frameCallBack = framedetector.detectFrames(raw_img);

    if(!frameCallBack.empty())
    {
        for(int i =0;i<frameCallBack.size();i++)
        {
            std::cout<<"frame number ="<<frameCallBack.size()<<std::endl;
            std::cout<<"frame callback =" << frameCallBack[i].getCorners() <<std::endl;
            std::cout<<"loop i ="<<i<<std::endl;

            Point_BOTTOM_LEFT.x = frameCallBack[i].getCorners()[0].x;
            Point_BOTTOM_LEFT.y = frameCallBack[i].getCorners()[0].y;
            Point_BOTTOM_RIGHT.x = frameCallBack[i].getCorners()[1].x;
            Point_BOTTOM_RIGHT.y = frameCallBack[i].getCorners()[1].y;
            Point_UP_RIGHT.x = frameCallBack[i].getCorners()[2].x;
            Point_UP_RIGHT.y = frameCallBack[i].getCorners()[2].y;
            Point_UP_LEFT.x = frameCallBack[i].getCorners()[3].x;
            Point_UP_LEFT.y = frameCallBack[i].getCorners()[3].y;


            //---------------------------------------------------------------------------------------------------------------------//

            p4psolver.SetCameraMatrix(fx,fy,cx,cy);
            //init distortion param
            p4psolver.SetDistortionCoefficients(e1, e2, e3, e4, e5);

            //----------------------------------------------------------------------------------------------------------------//
            if(frameCallBack[i].getID() == "CLOSEDGATE")
            {
                p4psolver.Points3D.push_back(cv::Point3f(-CLOSED_GATE_SIZE/2, -CLOSED_GATE_SIZE/2, 0));     //P1/m
                p4psolver.Points3D.push_back(cv::Point3f(CLOSED_GATE_SIZE/2, -CLOSED_GATE_SIZE/2, 0));   //P2/m
                p4psolver.Points3D.push_back(cv::Point3f(CLOSED_GATE_SIZE/2, CLOSED_GATE_SIZE/2, 0));   //P3/m
                //p4psolver.Points3D.push_back(cv::Point3f(150, 200, 0));   //P4/m
                p4psolver.Points3D.push_back(cv::Point3f(-CLOSED_GATE_SIZE/2, CLOSED_GATE_SIZE/2, 0)); //P5/m
            }
            if(frameCallBack[i].getID() == "jungle frame")
            {
                p4psolver.Points3D.push_back(cv::Point3f(0, 0, 0));     //P1/m
                p4psolver.Points3D.push_back(cv::Point3f(-1.0, 0, 0));   //P2/m
                p4psolver.Points3D.push_back(cv::Point3f(-1.0, 1.0, 0));   //P3/m
                //p4psolver.Points3D.push_back(cv::Point3f(150, 200, 0));   //P4/m
                p4psolver.Points3D.push_back(cv::Point3f(0, 1.0, 0)); //P5/m
            }
            if(frameCallBack[i].getID() == "unknown")
            {
                FramePoses.frame_id = "unknown";
                p4psolver.Points3D.push_back(cv::Point3f(0, 0, 0));     //P1/m
                p4psolver.Points3D.push_back(cv::Point3f(1.28, 0 ,0));   //P2/m
                p4psolver.Points3D.push_back(cv::Point3f(1.28, 1.4, 0));   //P3/m
                //p4psolver.Points3D.push_back(cv::Point3f(150, 200, 0));   //P4/m
                p4psolver.Points3D.push_back(cv::Point3f(0, 1.4, 0)); //P5/m
            }

            //------------------------------------------------------------------------------------------------------------------------//
            //init feature point picture frame
            p4psolver.Points2D.push_back(Point_UP_LEFT);   //P4
            p4psolver.Points2D.push_back(Point_UP_RIGHT);  //P1
            p4psolver.Points2D.push_back(Point_BOTTOM_RIGHT);  //P2
            p4psolver.Points2D.push_back(Point_BOTTOM_LEFT);  //P3

            std::cout<<  "Points3D_size ="<<p4psolver.Points3D.size()<<std::endl;
            std::cout << "points 2D" << p4psolver.Points2D << std::endl;

            // -----------------------------------------------------------------3 methods for solvepnp--------------------------------//

            p4psolver.Solve(PNPSolver::METHOD::CV_ITERATIVE);
            cv::Mat rotation_vec, translation_vec;
            p4psolver.getRotationAndTranslation(rotation_vec, translation_vec);

            //            double x = 0, y = 0, z = 0, roll = 0, pitch = 0, yaw = 0;
            //            p4psolver.convertToAerostackFrame(x, y, z, roll, pitch, yaw, rotation_vec, translation_vec);

            //            tf::Quaternion quaternion = tf::createQuaternionFromRPY(roll, pitch, yaw);

            //---------------------------------------------------------------------Rotation to Quaternion-----------------------------//

            cv::Mat RoteM;
            cv::Rodrigues(rotation_vec, RoteM);

            Eigen::Matrix3d Rmat_E;
            Rmat_E(0,0) = RoteM.at<double>(0,0);
            Rmat_E(0,1) = RoteM.at<double>(0,1);
            Rmat_E(0,2) = RoteM.at<double>(0,2);

            Rmat_E(1,0) = RoteM.at<double>(1,0);
            Rmat_E(1,1) = RoteM.at<double>(1,1);
            Rmat_E(1,2) = RoteM.at<double>(1,2);

            Rmat_E(2,0) = RoteM.at<double>(2,0);
            Rmat_E(2,1) = RoteM.at<double>(2,1);
            Rmat_E(2,2) = RoteM.at<double>(2,2);

            Eigen::Quaterniond quaternion(Rmat_E);
            tf::Quaternion quaterniond;
            tf::quaternionEigenToTF(quaternion,quaterniond);

            tf::Matrix3x3 m(quaterniond);

            //convert quaternion to euler angels
            double yaw, pitch, roll;
            m.getEulerYPR(yaw, pitch, roll);

            std::cout << "roll: " << roll
                      << "pitch: " << pitch
                      << "yaw: "  << yaw << std::endl;

            std::cout <<  "trans: " << translation_vec << std::endl;

            //publish
            geometry_msgs::PoseStamped Re_PNP;
            Re_PNP.header.frame_id = "cam_frame";
            Re_PNP.header.stamp    = ros::Time::now();
            Re_PNP.pose.position.x = translation_vec.at<double>(0,0);
            Re_PNP.pose.position.y = translation_vec.at<double>(1,0);
            Re_PNP.pose.position.z = translation_vec.at<double>(2,0);
            Re_PNP.pose.orientation.w = quaternion.w();
            Re_PNP.pose.orientation.x = quaternion.x();
            Re_PNP.pose.orientation.y = quaternion.y();
            Re_PNP.pose.orientation.z = quaternion.z();
            FramePoses.poses.push_back(Re_PNP);

            p4psolver.Points3D.clear();
            p4psolver.Points2D.clear();
        }

        //-----------------------------------------------------publish_frame---------------------------------------------------//

        //frame_points.publish(FramePoints);
        geometry_msgs::PointStamped PointsInFrame;
        //this first frame received is the biggest frame and that should be published only
        for(int j = 0; j<4; j++)
        {
            PointsInFrame.header.frame_id = "cam_frame";
            PointsInFrame.header.stamp    = ros::Time::now();
            PointsInFrame.point.x = frameCallBack[0].getCorners()[j].x;
            PointsInFrame.point.y = frameCallBack[0].getCorners()[j].y;
            PointsInFrame.point.z = 0;
            FramePoints.points.push_back(PointsInFrame);
        }
    }
    else
    {
        std::cout<<"no frame in the camera image!"<<std::endl;
    }

    if(!FramePoints.points.empty())
    {
        frame_points.publish(FramePoints);
        FramePoints.points.clear();
    }
    if(!FramePoses.poses.empty())
    {
        Re2pnp.publish(FramePoses);
    }
}


int main(int argc, char **argv)
{
    //Init
    ros::init(argc, argv, "PNP_solver"); //Say to ROS the name of the node and the parameters
    ros::NodeHandle n; //Este nodo admite argumentos!!
    ros::Subscriber pic_sub = n.subscribe("/hummingbird_adr1/camera_front/image_raw", 1, picCallback);
    ros::Subscriber camera_para = n.subscribe("/hummingbird_adr1/camera_front/camera_info",1,rgbCamInfoCallback);
    //ros::Subscriber aruco_detection = n.subscribe("/aruco_eye/aruco_observation",1, arucoDetectionCallback);

    Re2pnp =n.advertise<solvepnp::FramePoses>("/Re_PNP",1);
    frame_points =n.advertise<solvepnp::FramePoints>("/frame_points",1);

    ros::spin();

    return 1;
}
