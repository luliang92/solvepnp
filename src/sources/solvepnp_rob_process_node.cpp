#include "solvepnp_rob_process.h"


int main(int argc, char **argv)
{

    ros::init(argc, argv, ros::this_node::getName());

    std::string my_configfile;
    std::string my_stack_path_;
    std::string my_drone_id_;

    ros::param::get("~solvepnp_config_file",my_configfile);
    if(my_configfile.length() == 0)
    {
      my_configfile="frame_detector.xml";
    }

    ros::param::get("~stack_path_", my_stack_path_);
    if(my_stack_path_.length() == 0)
        my_stack_path_ = "~/workspace/ros/aerostack_catkin_ws/src/aerostack_stack";

    ros::param::get("~drone_id_int", my_drone_id_);
    if(my_drone_id_.length() == 0)
    {
      my_drone_id_="7";
    }

    solvepnp_rob_process my_solve_pnp(my_stack_path_+"/"+"configs/drone"+my_drone_id_+"/"+my_configfile);
    my_solve_pnp.setUp();

    //my_solve_pnp.start();
    ros::spin();




    return 0;
}

